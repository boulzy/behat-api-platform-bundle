<?php

namespace Boulzy\Tests\BehatApiPlatformBundle\Behat\Context;

use Assert\Assertion;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Hook\BeforeScenario;
use Behat\Step\Given;
use Behat\Step\Then;
use Behat\Step\When;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;
use Symfony\Component\Yaml\Yaml;

class TestContext implements Context
{
    private Filesystem $filesystem;
    private string $workingDir;

    private ?Process $process = null;

    public function __construct()
    {
        $this->filesystem = new Filesystem();
        $this->workingDir = \sys_get_temp_dir().'/'.\uniqid();
    }

    #[BeforeScenario]
    public function prepareWorkingDir(): void
    {
        $this->filesystem->mkdir($this->workingDir);
    }

    public function cleanUpWorkingDir(): void
    {
        $this->filesystem->remove($this->workingDir);
    }

    #[Given('/^an API Platform application with boulzy\/behat-api-platform-bundle enabled$/')]
    public function anAPIPlatformApplicationWithBoulzyBehatApiPlatformBundleEnabled(): void
    {
        $this->standardSymfonyAutoloaderConfigured();

        $this->thereIsFile('src/Kernel.php', <<<'CON'
<?php declare(strict_types=1);

namespace App;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel as HttpKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

class Kernel extends HttpKernel
{
    use MicroKernelTrait;

    public function registerBundles(): iterable
    {
        return [
            new \Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new \ApiPlatform\Symfony\Bundle\ApiPlatformBundle(),
            new \FriendsOfBehat\SymfonyExtension\Bundle\FriendsOfBehatSymfonyExtensionBundle(),
            new \Boulzy\BehatApiPlatformBundle\Bundle\BoulzyBehatApiPlatformBundle(),
        ];
    }

    protected function configureContainer(ContainerBuilder $container, LoaderInterface $loader): void
    {
        $container->loadFromExtension('framework', [
            'test' => $this->getEnvironment() === 'test',
            'secret' => '$eKr3t',
        ]);

        $container->loadFromExtension('api_platform', [
            'mapping' => [
                'paths' => [
                    '%kernel.project_dir%/DTO',
                ],
            ],
        ]);
    }
    
    protected function configureRoutes(RoutingConfigurator $routes): void
    {
        $routes->import('.', 'api_platform');
    }
}
CON
        );

        $this->thereIsFile(
            'src/DTO/Product.php', <<<'CON'
<?php declare(strict_types=1);

namespace App\DTO;

use ApiPlatform\Metadata\ApiResource;

#[ApiResource]
class Product // The class name will be used to name exposed resources
{
    public string $name = '';
}
CON
        );
    }

    #[Given('a standard Symfony autoloader configured')]
    public function standardSymfonyAutoloaderConfigured(): void
    {
        $this->thereIsFile('vendor/autoload.php', \sprintf(<<<'CON'
<?php declare(strict_types=1);

$loader = require '%s';
$loader->addPsr4('App\\', __DIR__ . '/../src/');
$loader->addPsr4('App\\Tests\\', __DIR__ . '/../tests/');

return $loader; 
CON
            , __DIR__.'/../../../vendor/autoload.php'));
    }

    #[Given('a Behat configuration containing:')]
    #[Given('a Behat configuration containing :configuration')]
    public function thereIsConfiguration(PyStringNode|string $configuration): void
    {
        $configuration = (string) $configuration;

        $behatConfigFile = "{$this->workingDir}/behat.yaml";
        $newConfigFile = "{$this->workingDir}/behat-".md5($configuration).'.yaml';

        $this->filesystem->dumpFile($newConfigFile, $configuration);
        if (!\file_exists($behatConfigFile)) {
            $this->filesystem->dumpFile($behatConfigFile, Yaml::dump(['imports' => []]));
        }

        $behatConfiguration = Yaml::parseFile($behatConfigFile);
        $behatConfiguration['imports'][] = $newConfigFile;
        $this->filesystem->dumpFile($behatConfigFile, Yaml::dump($behatConfiguration));
    }

    #[Given('the feature file :filename')]
    public function copyFeatureFile(string $filename): void
    {
        $this->filesystem->copy(__DIR__.'/features/'.$filename, "{$this->workingDir}/features/{$filename}");
    }

    #[Given('a file :filename containing:')]
    #[Given('a file :filename containing :content')]
    public function thereIsFile(string $filename, PyStringNode|string $content): void
    {
        $content = (string) $content;

        $path = "{$this->workingDir}/{$filename}";
        $this->filesystem->dumpFile($path, $content);
    }

    #[When('I run Behat')]
    public function iRunBehat(): void
    {
        echo BEHAT_BIN_PATH;
        $this->process = new Process([BEHAT_BIN_PATH, '--strict', '-vvv', '--no-interaction', '--config='.$this->workingDir.'/behat.yaml'], $this->workingDir);
        $this->process->start();
        $this->process->wait();
    }

    #[Then('I print the output')]
    public function iPrintTheOutput(): void
    {
        Assertion::notNull($this->process);

        echo $this->process->getOutput()."\n";
        echo $this->process->getErrorOutput()."\n";
    }

    #[Then('it should pass')]
    public function itShouldPass(): void
    {
        Assertion::notNull($this->process);
        Assertion::same(0, $this->process->getExitCode());
    }
}
