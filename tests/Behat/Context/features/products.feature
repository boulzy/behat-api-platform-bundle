Feature: API Platform
    Scenario: List products
        When I send a POST request to "/products" with body:
        """
        {
            "name": "Product name"
        }
        """
    Then the response status code is 201
    And the response body matches json:
    """
        {
            "@id": "@variableType(string)",
            "name": "Product name"
        }
    """