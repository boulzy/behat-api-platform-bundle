<?php

namespace Boulzy\Tests\BehatApiPlatformBundle\DependencyInjection;

use ApiPlatform\Symfony\Bundle\Test\Client;
use Boulzy\BehatApiPlatformBundle\Bundle\DependencyInjection\BoulzyBehatApiPlatformExtension;
use Boulzy\BehatApiPlatformBundle\Context\JsonApiContext;
use Boulzy\BehatApiPlatformBundle\Exception\NoTestClientException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Definition;

class BoulzyBehatApiPlatformExtensionTest extends TestCase
{
    private BoulzyBehatApiPlatformExtension $extension;

    private ContainerInterface $container;

    protected function setUp(): void
    {
        $this->extension = new BoulzyBehatApiPlatformExtension();
        $this->container = new ContainerBuilder();
        $this->container->getCompilerPassConfig()->setOptimizationPasses([]);
        $this->container->getCompilerPassConfig()->setRemovingPasses([]);
        $this->container->getCompilerPassConfig()->setAfterRemovingPasses([]);
        $this->container->registerExtension($this->extension);
    }

    public function testExtensionWithApiPlatformTestClient(): void
    {
        $this->container->setDefinition('test.api_platform.client', new Definition(Client::class));

        $this->compile();

        $this->assertTrue($this->container->hasDefinition(JsonApiContext::class));
        $definition = $this->container->getDefinition(JsonApiContext::class);
        $this->assertTrue($definition->hasMethodCall('setApiClient'));
        $this->assertTrue($definition->hasMethodCall('setArrayComparator'));
    }

    public function testExtensionWithoutApiPlatformTestClient(): void
    {
        $this->expectException(NoTestClientException::class);
        $this->expectExceptionMessageMatches('%The service "test.api_platform.client" does not exist%');
        $this->expectExceptionMessageMatches('%composer require --dev symfony/browser-kit symfony/http-client%');

        $this->compile();
    }

    private function compile(array $config = []): void
    {
        $this->extension->load($config, $this->container);
        $this->container->compile();
    }
}
