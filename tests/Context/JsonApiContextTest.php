<?php

namespace Boulzy\Tests\BehatApiPlatformBundle\Context;

use Boulzy\ArrayComparator\ArrayComparator;
use Boulzy\BehatApiPlatformBundle\Context\JsonApiContext;
use Boulzy\BehatApiPlatformBundle\Exception\NoResponseException;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class JsonApiContextTest extends TestCase
{
    private JsonApiContext $context;

    private HttpClientInterface $client;

    protected function setUp(): void
    {
        $this->client = $this->createMock(HttpClientInterface::class);
        $this->context = new JsonApiContext();
        $this->context->setApiClient($this->client);
        $this->context->setArrayComparator(new ArrayComparator());
    }

    public static function requestExamples(): iterable
    {
        yield ['GET', 'http://localhost/test'];
        yield ['POST', 'http://localhost/test', \json_encode(['key' => 'value'])];
    }

    public static function responseMethods(): iterable
    {
        yield ['theResponseIsSuccessful'];
        yield ['theResponseStatusCodeIs', ['expected' => 200]];
        yield ['theResponseHasHeader', ['header' => 'Content-Type']];
        yield ['theResponseBodyMatchesJson', ['{"key":"value"}']];
    }

    #[DataProvider('responseMethods')]
    public function testResponseMethodsWithoutRequest(string $method, array $args = []): void
    {
        $this->expectException(NoResponseException::class);

        $this->context->$method(...$args);
    }

    #[DataProvider('requestExamples')]
    public function testSendRequest(string $method, string $uri, string $body = null): void
    {
        $options = [
            'headers' => [
                'Content-Type' => ['application/ld+json'],
            ],
        ];

        if (null !== $body) {
            $options['body'] = $body;
        }

        $this->client
            ->expects($this->once())
            ->method('request')
            ->with($method, $uri, $options)
            ->willReturn($this->createStub(ResponseInterface::class))
        ;

        $this->context->iSendARequestTo($method, $uri, $body);
    }

    public function testTheResponseIsSuccessful(): void
    {
        $response = $this->createStub(ResponseInterface::class);
        $response->method('getStatusCode')->willReturn(200);

        $this->client
            ->expects($this->once())
            ->method('request')
            ->with('GET', 'http://localhost')
            ->willReturn($response)
        ;

        $this->context->iSendARequestTo('GET', 'http://localhost');

        $success = true;
        try {
            $this->context->theResponseIsSuccessful();
        } catch (\Throwable $e) {
        }
    }
}
