<?php

namespace Boulzy\BehatApiPlatformBundle\Bundle\DependencyInjection;

use Boulzy\ArrayComparator\ArrayComparator;
use Boulzy\BehatApiPlatformBundle\Context\ApiContext;
use Boulzy\BehatApiPlatformBundle\Context\JsonApiContext;
use Boulzy\BehatApiPlatformBundle\Exception\NoTestClientException;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Reference;

class BoulzyBehatApiPlatformExtension extends Extension implements CompilerPassInterface
{
    private const CONTEXT_TAG = 'boulzy_behat_api_platform.context';

    private const ARRAY_COMPARATOR = 'boulzy_behat_api_platform.array_comparator';

    private const TEST_CLIENT = 'test.api_platform.client';

    public function load(array $configs, ContainerBuilder $container)
    {
        $jsonApiContext = new Definition();
        $jsonApiContext->setPublic(true);
        $jsonApiContext->addTag(self::CONTEXT_TAG);

        $container->setDefinition(self::ARRAY_COMPARATOR, new Definition(ArrayComparator::class));
        $container->setDefinition(JsonApiContext::class, $jsonApiContext);
        $container->registerForAutoconfiguration(ApiContext::class)->addTag(self::CONTEXT_TAG);
    }

    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition(self::TEST_CLIENT)) {
            throw new NoTestClientException(<<<MES
The service "'.self::TEST_CLIENT.'" does not exist.
Check that you have done the configuration to run functional tests with API Platform.

This is usually done by running the following command:

composer require --dev symfony/browser-kit symfony/http-client
MES);
        }

        foreach ($container->findTaggedServiceIds(self::CONTEXT_TAG) as $serviceId => $attributes) {
            $serviceDefinition = $container->findDefinition($serviceId);
            $serviceDefinition->addMethodCall('setApiClient', [new Reference(self::TEST_CLIENT)]);
            $serviceDefinition->addMethodCall('setArrayComparator', [new Reference(self::ARRAY_COMPARATOR)]);
            $serviceDefinition->clearTag(self::CONTEXT_TAG);
        }
    }
}
