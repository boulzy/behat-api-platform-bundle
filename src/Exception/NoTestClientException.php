<?php

namespace Boulzy\BehatApiPlatformBundle\Exception;

class NoTestClientException extends \RuntimeException implements BehatApiPlatformException
{
}
