<?php

namespace Boulzy\BehatApiPlatformBundle\Exception;

use Assert\InvalidArgumentException;

class AssertionFailedException extends InvalidArgumentException implements BehatApiPlatformException
{
}
