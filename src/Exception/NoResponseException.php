<?php

namespace Boulzy\BehatApiPlatformBundle\Exception;

class NoResponseException extends \RuntimeException implements BehatApiPlatformException
{
}
