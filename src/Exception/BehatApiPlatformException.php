<?php

namespace Boulzy\BehatApiPlatformBundle\Exception;

interface BehatApiPlatformException extends \Throwable
{
}
