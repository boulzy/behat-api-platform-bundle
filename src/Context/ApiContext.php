<?php

namespace Boulzy\BehatApiPlatformBundle\Context;

use Behat\Behat\Context\Context;
use Boulzy\ArrayComparator\ArrayComparator;
use Symfony\Contracts\HttpClient\HttpClientInterface;

interface ApiContext extends Context
{
    public function setApiClient(HttpClientInterface $apiClient): void;

    public function setArrayComparator(ArrayComparator $arrayComparator): void;
}
