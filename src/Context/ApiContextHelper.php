<?php

namespace Boulzy\BehatApiPlatformBundle\Context;

use Boulzy\ArrayComparator\ArrayComparator;
use Boulzy\ArrayComparator\Exception\ArrayComparatorException;
use Boulzy\BehatApiPlatformBundle\Assertion\Assert;
use Boulzy\BehatApiPlatformBundle\Exception\NoResponseException;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

trait ApiContextHelper
{
    private HttpClientInterface $apiClient;

    private ArrayComparator $arrayComparator;

    private ?ResponseInterface $response = null;

    /**
     * @param array<string, string[]>|null $headers
     * @param mixed[]|string|null          $body
     */
    protected function send(string $method, string $url, array $headers = null, array|string $body = null): void
    {
        $options = [];

        if (null !== $body) {
            $options['body'] = $body;
        }

        if (null !== $headers) {
            $options['headers'] = $headers;
        }

        $this->response = $this->apiClient->request($method, $url, $options);
    }

    protected function getResponse(): ResponseInterface
    {
        if (!$this->response) {
            throw new NoResponseException('No response has been set. Did you make a request in a previous step?');
        }

        return $this->response;
    }

    protected function assertResponseIsSuccessful(string $message = ''): void
    {
        $statusCode = $this->getResponse()->getStatusCode();

        if (empty($message)) {
            $message = 'The response status code {{actual}} is not in the expected 20x range.';
        }
        $message = \str_replace('{{actual}}', (string) $statusCode, $message);

        Assert::true(200 <= $statusCode && 300 > $statusCode, $message);
    }

    protected function assertResponseStatusCodeIs(int $expected, string $message = ''): void
    {
        $statusCode = $this->getResponse()->getStatusCode();

        if (empty($message)) {
            $message = 'The response status code {{actual}} does not match expected {{expected}}.';
        }
        $message = \str_replace(['{{actual}}', '{{expected}}'], [$statusCode, $expected], $message);

        Assert::same($expected, $statusCode, $message);
    }

    protected function assertResponseHasHeader(string $header, string $expected = null, string $message = ''): void
    {
        $key = \strtolower($header);
        $headers = $this->getResponse()->getHeaders(false);

        if (empty($message)) {
            if (null === $expected) {
                $message = 'Response does not have HTTP header "'.$header.'"';
            } else {
                $message = 'Response HTTP header "'.$header.'" value "{{actual}}" does not match expected "{{expected}}"';
            }
        }
        $message = \str_replace(['{{actual}}', '{{expected}}'], [\implode(' ', $headers['key'] ?? []), $expected], $message);

        Assert::keyExists($headers, $key, $message);
        if (null !== $expected) {
            Assert::inArray($expected, $headers[$key], $message);
        }
    }

    protected function assertResponseBodyMatches(string $expected, string $message = ''): void
    {
        $responseBody = $this->getResponse()->getContent(false);

        $this->assertJsonMatches($expected, $responseBody, $message);
    }

    protected function assertJsonMatches(string $expected, string $actual, string $message = ''): void
    {
        if (null === $expected = \json_decode($expected, true)) {
            throw new \InvalidArgumentException('Argument $expected does not contain valid JSON');
        }

        if (null === $actual = \json_decode($actual, true)) {
            throw new \InvalidArgumentException('Argument $actual does not contain valid JSON');
        }

        $this->assertArrayMatches($expected, $actual, $message);
    }

    /**
     * @param mixed[] $expected
     * @param mixed[] $actual
     */
    protected function assertArrayMatches(array $expected, array $actual, string $message = ''): void
    {
        try {
            $result = $this->arrayComparator->compare($expected, $actual);
        } catch (ArrayComparatorException $e) {
            $result = false;
            if (empty($message)) {
                $message = $e->getMessage();
            }
        }

        Assert::true($result, $message);
    }

    public function setApiClient(HttpClientInterface $apiClient): void
    {
        $this->apiClient = $apiClient;
    }

    public function setArrayComparator(ArrayComparator $arrayComparator): void
    {
        $this->arrayComparator = $arrayComparator;
    }
}
