<?php

namespace Boulzy\BehatApiPlatformBundle\Context;

use Behat\Gherkin\Node\PyStringNode;
use Behat\Step\Then;
use Behat\Step\When;

class JsonApiContext implements ApiContext
{
    use ApiContextHelper {
        send as request;
    }

    /**
     * @var array<string,string[]>
     */
    protected array $requestHeaders = [
        'Content-Type' => ['application/ld+json'],
    ];

    #[When('/^I send a (GET|POST|PUT|PATCH|DELETE) request to "(.*)"$/')]
    #[When('/^I send a (GET|POST|PUT|PATCH|DELETE) request to "(.*)" with body:$/')]
    #[When('/^I send a (GET|POST|PUT|PATCH|DELETE) request to "(.*)" with body "(.*)"$/')]
    public function iSendARequestTo(string $method, string $uri, PyStringNode|string $body = null): void
    {
        if (null !== $body) {
            $body = (string) $body;
        }

        $this->send(
            method: $method,
            url: $uri,
            body: $body
        );
    }

    #[Then('/^the response is successful$/')]
    public function theResponseIsSuccessful(): void
    {
        $this->assertResponseIsSuccessful();
    }

    #[Then('/^the response status code is ([1-5][0-9]{2})$/')]
    public function theResponseStatusCodeIs(int $expected): void
    {
        $this->assertResponseStatusCodeIs($expected);
    }

    #[Then('/^the response has header "(.*)"$/')]
    #[Then('/^the response header "(.*)" is "(.+)"$/')]
    public function theResponseHasHeader(string $header, string $expected = null): void
    {
        $this->assertResponseHasHeader($header, $expected);
    }

    #[Then('/^the response body matches json:$/')]
    public function theResponseBodyMatchesJson(PyStringNode|string $expected): void
    {
        $this->assertResponseBodyMatches((string) $expected);
    }

    /**
     * @param array<string, string[]>|null $headers
     * @param mixed[]|string|null          $body
     */
    protected function send(string $method, string $url, array $headers = null, array|string $body = null): void
    {
        if (!empty($this->requestHeaders)) {
            $headers = [...$this->requestHeaders, ...($headers ?? [])];
        }

        $this->request($method, $url, $headers, $body);
    }
}
