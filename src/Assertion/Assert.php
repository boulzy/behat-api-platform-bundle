<?php

namespace Boulzy\BehatApiPlatformBundle\Assertion;

use Assert\Assertion;
use Boulzy\BehatApiPlatformBundle\Exception\AssertionFailedException;

class Assert extends Assertion
{
    protected static $exceptionClass = AssertionFailedException::class;
}
