Feature: Using the API Platform test HTTP client
    In order to test my API
    As a developer
    I want to use the HTTP client provided by API Platform in its PHPUnit integration

    Background:
        Given an API Platform application with boulzy/behat-api-platform-bundle enabled
        And a Behat configuration containing:
            """
            default:
                suites:
                    default:
                        contexts:
                            - Boulzy\BehatApiPlatformBundle\Context\JsonApiContext
                extensions:
                    FriendsOfBehat\SymfonyExtension:
                        kernel:
                            class: App\Kernel
                            environment: test
            """

    Scenario: Running an API test suite
        Given the feature file "products.feature"
        When I run Behat
        Then I print the output
        Then it should pass
