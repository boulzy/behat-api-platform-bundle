# boulzy/behat-api-platform-bundle

_**boulzy/behat-api-platform-bundle** is a [Symfony](https://symfony.com/) bundle
that provides an integration between [Behat](https://docs.behat.org) and
[API Platform](https://api-platform.com/)._

-----

It provides a [Behat context](https://behat.org/en/latest/user_guide/context.html)
with steps to test HTTP requests to JSON API endpoints that uses the same client
used by API Platform when running [unit tests](https://phpunit.de/) from an
`\ApiPlatform\Symfony\Bundle\Test\ApiTestCase` test.

## Documentation

- [Getting started](docs/index.md)
- [Installation](docs/install.md)
- [Configuration](docs/config.md)
- [Usage](docs/usage.md)

## License

This project is licensed under the [MIT License](LICENSE).