# Getting started

**boulzy/behat-api-platform-bundle** makes it easier to test an API Platform
project with Behat.

It works by using the `\ApiPlatform\Symfony\Bundle\Test\Client` HTTP client
used by API Platform when running tests with PHPUnit and the
`\ApiPlatform\Symfony\Bundle\Test\ApiTestCase`.

You can use the `\Boulzy\BehatApiPlatformBundle\Context\JsonApiContext` provided
by this bundle to write HTTP requests based scenarios, extend it with your own
context class to adapt it to your API requirements or implement the
`\Boulzy\BehatApiPlatformBundle\Context\ApiContext` interface and write your own
step definitions using the API Platform client.

This bundle requires [friends-of-behat/symfony-extension](https://github.com/FriendsOfBehat/SymfonyExtension),
a Behat extension that allows to use your contexts as Symfony services.

[⬅️ README](../README.md) | [Installation ➡️](./install.md)
